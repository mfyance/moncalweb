<?php include('header.html') ?>
<section class="cleaner row main--content">
    <div class="cleaner col-container">
        <div class="large-12 columns tac mb--20">
            <h2>Nuestros Servicios</h2>
        </div>
        <div class="cleaner large-12 columns sec-descrp mb--20">
            <p>Entendemos las necesidades fusionadas de nuestros clientes y ofrecemos soluciones independientes e integradas.</p>
        </div>
    </div>
    <div class="cleaner  col-container">
        <div class="row cleaner">
            <div class="small-12 medium-4 columns wow slideInUp" style="visibility: visible; animation-name: slideInUp;">
                <div class="box--services hov-big">
                    <div class="service--pic cleaner pr" style="background-image: url(img/services/lavanderia.jpg)">
                        <div class="bg--pic cleaner"></div>
                        <h3 class="other">Lavandería Industrial</h3>
                    </div>
                    <ul class="cleaner detalle--servicios">
                        <li>Lavado con equipo industrial</li>
                        <li>Insumos de calidad comprobada</li>
                        <li>Puntualidad en las entregas</li>
                        <li>Personal calificado</li>
                    </ul>
                </div>
            </div>
            <div class="small-12 medium-4 columns wow slideInUp" style="visibility: visible; animation-name: slideInUp;">   
                <div class="box--services hov-big">
                    <div class="service--pic cleaner pr" style="background-image: url(img/services/costura.jpg)">
                        <div class="bg--pic cleaner"></div>
                        <h3 class="other">Confección</h3>
                    </div>
                    <ul class="cleaner detalle--servicios">
                        <li>Confección industrial de todo tipo de prendas</li>
                        <li>Conocimiento de las necesidades del cliente</li>
                        <li>Calidad y precisión</li>
                        <li>Servicio garantizado</li>
                    </ul>
                </div>
            </div>
            <div class="small-12 medium-4 columns wow slideInUp" style="visibility: visible; animation-name: slideInUp;">
                <div class="box--services hov-big">
                    <div class="service--pic cleaner pr" style="background-image: url(img/services/roperia.jpg)">
                        <div class="bg--pic cleaner"></div>
                        <h3 class="other">Gestión de Ropería</h3>
                    </div>
                    <ul class="cleaner detalle--servicios">
                        <li>Control permanente de flujo de prendas</li>
                        <li>Exactitud de inventarios</li>
                        <li>Control de reposición de prendas</li>
                        <li>Personal calificado</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="cleaner buttc tac mt--20"> 
        <a href="contacto.php" class="btn btn-1">
            <svg><rect x="0" y="0" fill="none" width="100%" height="100%"></rect></svg>Contactar ahora
        </a>
    </div>
</section>
<?php include('footer.html') ?>