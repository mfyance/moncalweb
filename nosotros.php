<?php include('header.html') ?>
<section class="cleaner row main--content">
    <div class="cleaner col-container">
        <div class="large-12 columns tac mb--20">
            <h2>¿Por qué elegirnos?</h2>
        </div>
        <div class="cleaner large-12 columns sec-descrp mb--20">
            <p>Nos basamos en 3 pilares importantes que nos identifican como empresa responsable y comprometida.</p>
        </div>
    </div>
    <div class="cleaner col-container">
        <div class="services cleaner">
            <div class="service--block cleaner">
                <div class="service--img cleaner">
                    <div class="cleaner pr">
                        <img src="img/interno/rrhh.jpg" alt="Recursos Humanos">
                        <div class="service--capa"></div>
                        <div class="service--title">
                            <h4 class="other">Recursos Humanos</h4>
                        </div>
                    </div>
                </div>
                <div class="service--text cleaner">
                    <ul>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="0.5s" data-wow-delay="1s">Capacitaciones trimestrales</li>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="1s" data-wow-delay="1s">Actividades motivacionales</li>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="1.5s" data-wow-delay="1s">****</li>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="2s" data-wow-delay="1s">****</li>
                    </ul>
                </div>
            </div>
            <div class="service--block cleaner">
                <div class="service--img cleaner">
                    <div class="cleaner pr">
                        <img src="img/interno/calidad.jpg" alt="Calidad">
                        <div class="service--capa"></div>
                        <div class="service--title">
                            <h4 class="other">Calidad</h4>
                        </div>
                    </div>
                </div>
                <div class="service--text cleaner">
                    <ul>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="0.5s" data-wow-delay="1s">Experiencia innovando</li>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="1s" data-wow-delay="1s">Precisión en lotes de confección</li>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="1.5s" data-wow-delay="1s">Eficiente control de inventarios</li>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="2s" data-wow-delay="1s">****</li>
                    </ul>
                </div>
            </div>
            <div class="service--block cleaner">
                <div class="service--img cleaner">
                    <div class="cleaner pr">
                        <img src="img/interno/infra.jpg" alt="Infraestructura">
                        <div class="service--capa"></div>
                        <div class="service--title">
                            <h4 class="other">Infraestructura</h4>
                        </div>
                    </div>
                </div>
                <div class="service--text cleaner">
                    <ul>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="0.5s" data-wow-delay="1s">Equipamiento tecnológico</li>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="1s" data-wow-delay="1s">Sistemas de contingencia</li>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="1.5s" data-wow-delay="1s">****</li>
                        <li class="wow fadeIn" data-wow-offset="10" data-wow-duration="2s" data-wow-delay="1s">****</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="cleaner buttc tac mt--20"> 
        <a href="contacto.php" class="btn btn-1">
            <svg><rect x="0" y="0" fill="none" width="100%" height="100%"></rect></svg>Contactar ahora
        </a>
    </div>
</section>
<?php include('footer.html') ?>