<?php include('header.html') ?>
<section id="one--block" class="cleaner pr">
    <div class="slider--title tac wow fadeIn" data-wow-offset="10" data-wow-duration="2s" data-wow-delay="1s">
        <h1><strong id="element"><i>Lavandería Industrial Moncal. Especialistas en Lavado y Servicio Integral.</i></strong></h1>
        <ul>
            <li class="list" focus-next>
                <span class="typeit typeit-box3"></span>
            </li>
            <li class="list" focus-next>
                <span class="typeit typeit-box4"></span>
            </li>
            <li class="list" focus-next>
                <span class="typeit typeit-box5" contenteditable></span>
            </li>
        </ul>
    </div>
    <div class="pr cleaner block--imagen zoomClass" style="background-image:url(img/slider/1.jpg)">
        <div class="capa cleaner"></div>                
    </div>
</section>
<section id="two--block" class="cleaner pr" style="background-color: rgba(93, 229, 247, 0.25)">
    <div class="container cleaner container--block">
        <div class="block--title cleaner">
            <h2>¿Por qué Elegirnos?</h2>
        </div>
        <div class="block--subtitle tac">
            <p>Nos basamos en 3 pilares importantes que nos identifican como empresa responsable y comprometida.</p>
        </div>
        <div class="cleaner us col-container">
            <div class="us--block cleaner wow slideInUp" data-wow-offset="10" data-wow-duration="0.5s" data-wow-delay="1s">
                <div class="us--ico cleaner tac">
                    <div class="us--ico-circle cleaner">
                        <img src="img/us/1.png" alt="Calidad" class="us--ico-img">
                    </div>                          
                </div>
                <div class="us--text">      
                    <div class="us--title tac"><h3>Recursos Humanos</h3></div>                  
                    <p>Realizamos capacitaciones y evaluaciones trimestrales a todos los involucrados en el proceso de nuestro servicio. Generemos actividades motivacionales para todo nuestro personal.</p>
                </div>
            </div>
            <div class="us--block cleaner wow slideInUp" data-wow-offset="10" data-wow-duration="1.5s" data-wow-delay="1s">
                <div class="us--ico cleaner tac">
                    <div class="us--ico-circle cleaner">
                        <img src="img/us/2.png" alt="Calidad" class="us--ico-img">
                    </div>                          
                </div>
                <div class="us--text">      
                    <div class="us--title tac"><h3>Calidad</h3></div>                   
                    <p>Más de 20 años de experiencia innovando en procedimientos de lavado, precisión en lotes de confección y eficiente control de los inventarios.</p>
                </div>
            </div>
            <div class="us--block cleaner wow slideInUp" data-wow-offset="10" data-wow-duration="1s" data-wow-delay="1s">
                <div class="us--ico cleaner tac">
                    <div class="us--ico-circle cleaner">
                        <img src="img/us/4.png" alt="Calidad" class="us--ico-img">
                    </div>                          
                </div>
                <div class="us--text">      
                    <div class="us--title tac"><h3>Infraestructura</h3></div>                   
                    <p>Equipamiento tecnológico de acuerdo al rubro de nuestros clientes con todos los sistemas de contingencia de rigor para atender los 365 días del año de manera interrumpida.</p>
                </div>
            </div>
        </div>
        <div class="cleaner buttc tac mt--20"> 
            <a href="nosotros.php" class="btn btn-1">
                <svg><rect x="0" y="0" fill="none" width="100%" height="100%"></rect></svg>Ver más
            </a>
        </div>
    </div>
</section>
<section class="row sec-pad cont-pq">
    <div class="container cleaner container--block">
        <div class="block--title cleaner">
            <h2>Nuestros Servicios</h2>
        </div>
        <div class="block--subtitle tac">
            <p>Entendemos las necesidades fusionadas de nuestros clientes y ofrecemos soluciones independientes e integradas.</p>
        </div>
        <div class="cleaner  col-container">
            <div class="row cleaner">
                <div class="small-12 medium-4 columns wow slideInUp">
                    <div class="box--services hov-big">
                        <div class="service--pic cleaner pr" style="background-image: url(img/services/lavanderia.jpg)">
                            <div class="bg--pic cleaner"></div>
                            <h3 class="other">Lavandería Industrial</h3>
                        </div>
                        <ul class="cleaner detalle--servicios">
                            <li>Lavado con equipo industrial</li>
                            <li>Insumos de calidad comprobada</li>
                            <li>Puntualidad en las entregas</li>
                            <li>Personal calificado</li>
                        </ul>
                    </div>
                </div>
                <div class="small-12 medium-4 columns wow slideInUp">   
                    <div class="box--services hov-big">
                        <div class="service--pic cleaner pr" style="background-image: url(img/services/costura.jpg)">
                            <div class="bg--pic cleaner"></div>
                            <h3 class="other">Confección</h3>
                        </div>
                        <ul class="cleaner detalle--servicios">
                            <li>Confección industrial de todo tipo de prendas</li>
                            <li>Conocimiento de las necesidades del cliente</li>
                            <li>Calidad y precisión</li>
                            <li>Servicio garantizado</li>
                        </ul>
                    </div>
                </div>
                <div class="small-12 medium-4 columns wow slideInUp">
                    <div class="box--services hov-big">
                        <div class="service--pic cleaner pr" style="background-image: url(img/services/roperia.jpg)">
                            <div class="bg--pic cleaner"></div>
                            <h3 class="other">Gestión de Ropería</h3>
                        </div>
                        <ul class="cleaner detalle--servicios">
                            <li>Control permanente de flujo de prendas</li>
                            <li>Exactitud de inventarios</li>
                            <li>Control de reposición de prendas</li>
                            <li>Personal calificado</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>        
        <div class="cleaner buttc tac mt--20"> 
            <a href="servicios.php" class="btn btn-1">
                <svg><rect x="0" y="0" fill="none" width="100%" height="100%"></rect></svg>Ver más
            </a>
        </div>
    </div>    
</section>
<section class="row proser">
    <div class="small-12 medium-4 columns heighpsd contdfgt cont-sbl bg--sectores" style="background-image: url('img/otros/clinica.jpg');">
        <div class="contser ">
            <h3>Sectores Especializados</h3>
        </div>
    </div>
    <div class="small-12 medium-8 columns ">
        <p class="mt--50">Nuestra experiencia abarca en los principales sectores como:</p>
        <h4 class="mt--20 other"><span class="sectores--list">Clínicas</span> <span class="sectores--list">Hospitales</span><span class="sectores--list">Hoteles</span><span class="sectores--list">Laboratorios</span><span class="sectores--list">Industria Masiva</span></h4>
    </div>    
</section>
<section class="row sec-testimonio parallax" style="background-image: url('img/banner.jpg')">
    <div class="col-container">
        <h2>Testimonios</h2>
        <div id="carr-test" class="cont-testt">
            <div class="item-test">
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nulla vitae elit libero, a pharetra augue.</p>
                <div class="dattest">
                    <div class="aligdb">
                        <img src="img/tesim.jpg">
                    </div>
                    <div class="aligdb">
                        <h5>JOHN SMITH</h5>
                        <span>CEO Of Apple</span>
                    </div>
                </div>
            </div>
            <div class="item-test">
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nulla vitae elit libero, a pharetra augue.</p>
                <div class="dattest">
                    <div class="aligdb">
                        <img src="img/tesim.jpg">
                    </div>
                    <div class="aligdb">
                        <h5>JOHN SMITH</h5>
                        <span>CEO Of Apple</span>
                    </div>
                </div>
            </div>
            <div class="item-test">
                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nulla vitae elit libero, a pharetra augue.</p>
                <div class="dattest">
                    <div class="aligdb">
                        <img src="img/tesim.jpg">
                    </div>
                    <div class="aligdb">
                        <h5>JOHN SMITH</h5>
                        <span>CEO Of Apple</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('footer.html') ?>