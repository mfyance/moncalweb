window.iniciarGMap = function() {
    var styles = [{
        stylers: [{
            hue: "#148dbf"
        }, {
            "saturation": -10
        }, {
            "lightness": 10
        }]
    }, {
        featureType: "road",
        elementType: "geometry",
        stylers: [{
            lightness: 10
        }, {
            visibility: "simplified"
        }]
    }, {
        featureType: "road",
        elementType: "labels",
        stylers: [{
            visibility: "on"
        }]
    }];

    var styledMap = new google.maps.StyledMapType(styles, {
        name: "Ania"
    });

    // Edit Ubication
    var miUbicacion = new google.maps.LatLng(-12.1749000, -77.0006999);
    // End Edi

    var mapOptions = {
        zoom: 17,
        center: miUbicacion,
        scrollwheel: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
    };
    var map = new google.maps.Map(document.getElementById('load-gmap'), mapOptions);

    var contentString =
        '<div id="gm--message" class="cleaner">' +
        '<a class="gm--capa" href="https://goo.gl/cyy5xP" target="_blank"><span><i class="ico1 material-icons ">insert_emoticon</i></span></a>' +
        '<div class="top cleaner"><div class="cleaner tac"><img src="img/empresa-moncal.jpg" alt=""></div></div></div>' +
        '</div>' +
        '</div>' +
        '<div class="cleaner tac"><strong>Lavandería Moncal</strong></div>'

    var infowindow = new google.maps.InfoWindow({
        content: contentString,
        Width: 300
    });
    var marker = new google.maps.Marker({
        position: miUbicacion,
        map: map,
        title: "Lavandería Moncal",
        icon: 'img/place.png',
        animation: google.maps.Animation.DROP
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
}

var app = (function($, undefined) {
    "use strict";
    var events, suscribeEvents, beforeCatchDom, catchDom, dom, afterCatchDom, fn, initialize, st;
    st = {
        form: "#frmContacto",
        field: ".form__field",
        input               : "input[type='text']",
        textarea            : "textarea",
        select              : "select"
    };
    dom = {};
    beforeCatchDom = function() {

    };
    catchDom = function() {
        dom.form = $(st.form)
        dom.field = $(st.field)
    };
    afterCatchDom = function() {
        fn.countPage()
        fn.animatedText()
        fn.configValidate();
        fn.sliderTestimonios();
        // fn.validationForm();
        //fn.loadGmaps()
    };
    suscribeEvents = function() {
        $('.menu--link').on('click', events.menuLinks)
    };
    events = {
        menuLinks: function() {
            var link = $(this).data("href")
            $('html, body').animate({
                scrollTop: $(link).offset().top
            }, 2000);
        }
    };
    fn = {
    cleanAllFields: function(content, time) {
            var t = (time != undefined) ? time : 0
            setTimeout(function () {
                if(content.find(st.input).length){
                    $.each(content.find(st.input) , function(i,el){
                        $(this).val('')
                    })
                }
                if(content.find(st.textarea).length){
                    $.each(content.find(st.textarea) , function(i,el){
                        $(this).val('')
                    })
                }
                if(content.find(st.select).length){
                    $.each(content.find(st.select) , function(i,el){
                        $(this).find('option[value="default"]').prop("selected", true)
                    })
                }
            }, t)
        },
        countPage: function() {
            $("#countdown").countdown({
                    date: "30 january 2018 12:00:00",
                    format: "on"
                },
                function() {
                    // callback function
                });
        },
        animatedText: function() {
            var wow = new WOW({
                boxClass: 'wow', // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset: 0, // distance to the element when triggering the animation (default is 0)
                mobile: true, // trigger animations on mobile devices (default is true)
                live: true, // act on asynchronously loaded content (default is true)
                callback: function(box) {
                    fn.configTexto($('#element'))
                       
                },
                scrollContainer: null // optional scroll container selector, otherwise use window
            });
            wow.init();
        },
        configTexto: function(element){
            element.typeIt({
                            strings: 'Lavamos',
                            typeSpeed: 100,
                            autoStart: true,
                            loop: true
                        })
                        .tiBreak()
                        .tiPause(750)
                        .tiType('Confeccionamos')
                        .tiBreak()
                        .tiPause(750)
                        .tiType('Gestionamos')
                        .tiPause(750)
                        .tiPause(5000)
                        .tiDelete()
                        .tiType('Calidad de lavado')
                        .tiBreak()
                        .tiPause(750)
                        .tiType('Lotes de confección precisos')
                        .tiBreak()
                        .tiPause(750)
                        .tiType('Personal capacitado ')
                        .tiPause(5000)
                        .tiDelete()              
              
        },
        configValidate: function() {
            $.validator.addMethod("select", function(value, element, arg){
                return arg != value;
            }, ""); 
            $.validator.setDefaults({
                errorClass: 'form_field_em',
                errorElement: 'em',
                errorPlacement: function(error, element) {
                    if (element.is(':radio')) {
                        error.appendTo(element.closest(dom.field));
                    } else if (element.is(':checkbox')) {
                        error.appendTo(element.closest(dom.field));
                    } else {
                        error.appendTo(element.closest(dom.field));
                    }
                },
                showErrors: function(errorMap, errorList) {
                    console.log('Se presentaros ' + this.numberOfInvalids() + ' errores');
                    this.defaultShowErrors();
                },
                unhighlight: function(element, errorClass, validClass) {
                    var numberOfInvalids;
                    numberOfInvalids = this.numberOfInvalids();
                    if (numberOfInvalids === 0) {
                        dom.form.removeClass('no_errores');
                    } else {
                        dom.form.addClass('si_errores');
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).closest(dom.field).removeClass('success').addClass('error');
                },
                success: function(element, errorClass, validClass) {
                    $(element).closest(dom.field).removeClass('error').addClass('success');
                }
            });
        },
        validationForm: function() {
            dom.form.validate({
                ignore: ":not(:visible)",
                debug: true,
                rules: {
                    nombres: {
                        required: true
                    },
                    correo: {
                        required: true,
                        email: true
                    },
                    telefono: {
                        required: true,
                        digits: true
                    },
                    servicio: {
                        required: true,
                        select: 'default'
                    }
                },
                messages: {
                    nombres: "Porfavor, ingrese sus nombres",
                    correo: "Porfavor, ingrese su correo",
                    telefono: "Porfavor, ingrese un teléfono",
                    servicios: "Elegir una opción"
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: 'contacto.php',
                        type: 'post',
                        data: $(form).serialize(),
                        beforeSend: function()
                        {   
                           $('.btn--enviar').html('<img class="img--loading" src="img/loading.gif" alt="">&nbsp;Enviando ...');
                        },
                    }).done(function(response) {
                        $.post("savecontact.php", $(form).serialize() ,
                            function(data, status){
                                if (data== 'success') {                            
                                    swal({
                                        title: "Mensaje Enviado",
                                        text: "En breve, un equipo de Moncal estará contactándose con usted",
                                        timer: 2000,
                                        type: "success",
                                    }, function(){
                                        fn.cleanAllFields($('form'))
                                    });
                                } else {
                                    swal("Upss", "Tenemos problemas ahora, vuelve a intentarlo más tarde", "warning")
                                }
                            }
                        );                      
                        
                    }).fail(function() {
                        swal("Upss", "Tenemos problemas ahora, vuelve a intentarlo más tarde", "warning")
                    }).always(function(){
                       $('.btn--enviar').html('Enviar&nbsp;<i class="ico1 material-icons">send</i>');
                    });
                    return false
                }
            });
        },
        sliderTestimonios: function() {
            var owl = $("#carr-test");
            if(owl.length){
                owl.owlCarousel({
                    autoPlay: true,
                    items : 1,
                    loop: true,
                    navigation: false,
                    pagination: true,
                    navigationText: ["",""],
                    responsiveClass:true,
                    autoWidth:true,
                    itemsDesktop : [1199,1],
                    itemsDesktopSmall : [979,1]
                });
                $('.owl-next').html('')
                $('.owl-prev').html('')
            }
        }
    };
    initialize = function() {
        beforeCatchDom();
        catchDom();
        afterCatchDom();
        suscribeEvents();
    };
    return {
        init: initialize
    };
})(jQuery);
app.init();