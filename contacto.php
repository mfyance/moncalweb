<?php include('header.html') ?>
<section class="row main--content">
	<div class="col-container cleaner">
        <div class="block--title cleaner">
			<h2>Formulario de Contacto</h2>
		</div>
		<div class="block--subtitle tac">
			<p>Escríbenos para así poder comunicarnos muy pronto.</p>
		</div>
		<div class="row">
			<div class="small-12 medium-6 columns">
				<div class="cont-form form-wh">
					<form>
						<div class="cont-input">
							<label>Nombres completos / Empresa</label>
							<input type="text">	
						</div>
						<div class="cont-input">
							<label>Correo</label>
							<input type="text">	
						</div>
						<div class="cont-input">
							<label>Teléfono</label>
							<input type="text">	
						</div>
						<div class="cont-input">
							<label>Servicio</label>
							<select>
								<option>Lavandería convencional</option>
								<option>Confección de ropas</option>
								<option>Gestión de ropería</option>
								<option>Lavandería + Confección</option>
								<option>Lavandería + Confección + Gestión de ropería y almacenes</option>
								<option>Otros</option>
							</select>	
						</div>
						<div class="cont-input">
							<label>Mensaje</label>
							<textarea></textarea>	
						</div>
		                <div class="butt-form tac cleaner">
		                	<button type="submit" class="head-button">Enviar&nbsp;<i class="ico1 material-icons">send</i></button>
		                </div>	
					</form>
				</div>
			</div>
			<div class="small-12 medium-6 columns seco-con">
				<div class="cont-info">
					<h3>Datos de contacto</h3>
					<h4><i class="fa fa-map-marker" aria-hidden="true"></i>Direcciones</h4>
					<p>Lima: Av. El Sol Cuadra 9 MZ J LT 13 - La Campiña - Chorrillos</p>
					<p>Piura: Calle Cuzco 549</p>
					<h4><i class="fa fa-phone" aria-hidden="true"></i>Teléfonos</h4>
					<p>Lima: (+511) 252 7076</p>
					<p>Piura: (+511) 924 391 568</p>
					<h4><i class="fa fa-envelope" aria-hidden="true"></i>Correo</h4>
					<p>contacto@moncal.pe</p>	
					<h3>Síguenos también en</h3>
					<ul class="redes">
						<!-- <li>
							<a href="https://www.facebook.com/lavanderiamoncal" target="_blank"><div class="redes--ico redes--ico-facebook"></div> </a>
						</li> -->
						<li>
							<a href="https://www.linkedin.com/company/lavandería-moncal" target="_blank"><div class="redes--ico redes--ico-linkedin"></div></a>
						</li>
						<li>
							<a href="https://plus.google.com/b/116140211069157998448" target="_blank"><div class="redes--ico redes--ico-plus"></div></a>
						</li>
					</ul>
				</div>
			</div>	
		</div>
	</div>
</section>
<section class="row">
	<div id="load-gmap" class="cleaner" style="width:100%;height:450px"></div>
</section>		
<?php include('footer.html') ?>